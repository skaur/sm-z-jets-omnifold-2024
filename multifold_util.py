import numpy as np 
import vector
from collections import OrderedDict
from tqdm import tqdm
import matplotlib.pyplot as plt
from matplotlib.collections import PatchCollection
from matplotlib.patches import Rectangle
from multifold_util import *
from matplotlib import rcParams
import matplotlib.mlab as mlab
import matplotlib.ticker as ticker
from matplotlib.ticker import FuncFormatter
from matplotlib.ticker import MaxNLocator
from matplotlib.colors import LogNorm
from matplotlib import rc
import matplotlib.gridspec as gridspec

lumi = 138.96
stylepath = './atlas.mplstyle'

plt.rcParams.update({
   "text.usetex": True,
   'text.latex.preamble': r'\usepackage{amsmath}',
   "pgf.rcfonts": False,
   "font.family": "serif",
   "font.size": 8,
   "xtick.labelsize": 8,
   "ytick.labelsize": 8,
   "legend.fontsize": 8
})


def plot_setup():
    # plt.style.use(stylepath)
    rcParams['pdf.fonttype'] = 42
    # dictionaries to hold the styles for re-use
    text_fd = dict(ha='left', va='center')
    atlas_fd = dict(weight='bold', style='italic', size=10, **text_fd)
    internal_fd = dict(size=10, **text_fd)
    text_fd = dict(size=8, **text_fd)


# Method that calculates a covariance matrix based on one of two tytpes of binned inputs:
# 1. a list of Hessian uncertainty variations (aka 'nuisance parameters')
#    this is activated when there is no third argument
#    as usual, such uncertainty components are uncorrelated wrt each other but fully correlated across bins
# 2. Bootstrap variations (aka resamplings), which is activated when uncerts_mean is defined
#    These uncertainties have a magnitude corresponding to the sample covariance
def fill_cov_matrix(n, uncerts_list, uncerts_mean=None): 
    v = np.zeros((n,n), float)
    for i in range(n):
        for j in range(n):
            for k in range(len(uncerts_list)):
                if uncerts_mean is not None: 
                    v[i,j] += 1/(len(uncerts_list)-1)*(uncerts_list[k][i]-uncerts_mean[i])*(uncerts_list[k][j]-uncerts_mean[j])
                else:
                    v[i,j] += uncerts_list[k][i]*uncerts_list[k][j]   
    return v 

# returns the systematic uncertainty in bins of an observable in percent
def calculate_uncertainty(df, observable, bins, systs):
    total_uncert = np.zeros(len(bins)-1)
    nom, _ = np.histogram(df[observable], bins=bins, weights=df.weights_nominal)
    for syst_name in systs:
        syst, _ = np.histogram(df[observable], bins=bins, weights=df[syst_name])
        total_uncert += (syst-nom)**2
    final_uncert = 100*np.sqrt(total_uncert)/nom
    return final_uncert

# returns stochastic uncertainty based on boostrap variations ('resampling')
def calculate_stat_uncertainty(df, observable, bins, bs_vars):
    stat = []
    nom, _ = np.histogram(df[observable], bins=bins, weights=df.weights_nominal)    
    for bs_name in bs_vars:
        varHist, _ = np.histogram(df[observable], bins=bins, weights=df[bs_name])
        stat.append(varHist)
    final_stat = 100*np.std(stat, axis=0)/nom
    if bs_vars[0].startswith('weights_ensemble'): # NN initialization
        # Since we take the median, we need the std err on the median, which is 1.253*std err on the mean
        return 1.253*final_stat
    else:
        return final_stat

# Effective number of events - i.e. effective statstics of a weighted dataset
# if all events have the same weight, this is the same as the number of events in the dataset
def nEff(weights):
    return (sum(weights))**2 / sum(weights**2)

def equal_effective_events_bins(df, var, N):
    # Sort the df by values
    df = df.sort_values(by=var)
    
    # Determine the number bins 
    # (note that weight_mc is already included in weights_nominal)
    total_weight = nEff(df.weights_nominal)
    n_bins = int(total_weight / N)
    print(f"Dividing into {n_bins} bins of {N} effective events.")

    # Initialize variables
    bin_edges = [df[var].iloc[0]]
    bin_index = 0
    cumulative_sum_weights = 0
    cumulative_sum_weights_squared = 0

    # Iterate through the sorted df to create bin edges
    for index, row in tqdm(df.iterrows(), total=len(df)):
        weight = row['weights_nominal']
        cumulative_sum_weights += weight
        cumulative_sum_weights_squared += weight ** 2
        n_eff = cumulative_sum_weights ** 2 / cumulative_sum_weights_squared
        if n_eff >= (bin_index + 1) * N and bin_index < n_bins - 1:
            # We have enough cumulative weight for this bin
            # Move to the next bin
            bin_edges.append(row[var])
            bin_index += 1
            
    # add the last index
    bin_edges.append(df[var].max())

    return bin_edges

ibu_bins = OrderedDict()
ibu_bins["pT_l1"] = [25.0, 125.0, 200.0, 300.0, 400.0, 500.0, 800.0]
ibu_bins["pT_l2"] = [25.0, 50.0, 75.0, 100.0, 150.0, 200.0, 400.0]
ibu_bins["eta_l1"] = [-2.5, -2.0, -1.5, -1.0, -0.75, -0.5, -0.25, 0.0, 0.25, 0.5, 0.75, 1.0, 1.5, 2.0, 2.5]
ibu_bins["eta_l2"] = [-2.5, -2.0, -1.5, -1.0, -0.75, -0.5, -0.25, 0.0, 0.25, 0.5, 0.75, 1.0, 1.5, 2.0, 2.5]
ibu_bins["phi_l1"] = [-(np.pi + 1e-5), -2.8, -2.4, -2.0, -1.6, -1.2, -0.8, -0.4, 0.0, 0.4, 0.8, 1.2, 1.6, 2.0, 2.4, 2.8, (np.pi + 1e-5)]
ibu_bins["phi_l2"] = [-(np.pi + 1e-5), -2.8, -2.4, -2.0, -1.6, -1.2, -0.8, -0.4, 0.0, 0.4, 0.8, 1.2, 1.6, 2.0, 2.4, 2.8, (np.pi + 1e-5)]
ibu_bins["pT_trackj1"] = [5.0, 50.0, 100.0, 150.0, 200.0, 300.0, 1000.0]
ibu_bins["pT_trackj2"] = [5.0, 25.0, 50.0, 100.0, 500.0]
ibu_bins["y_trackj1"] = [-2.5, -2.0, -1.75, -1.5, -1.25, -1.0, -0.75, -0.5, -0.25, 0.0, 0.25, 0.5, 0.75, 1.0, 1.25, 1.5, 1.75, 2.0, 2.5]
ibu_bins["y_trackj2"] = [-2.5, -2.0, -1.5, -1.0, -0.5, 0.0, 0.5, 1.0, 1.5, 2.0, 2.5]
ibu_bins["phi_trackj1"] = [-(np.pi + 1e-5), -2.8, -2.4, -2.0, -1.6, -1.2, -0.8, -0.4, 0.0, 0.4, 0.8, 1.2, 1.6, 2.0, 2.4, 2.8, (np.pi + 1e-5)]
ibu_bins["phi_trackj2"] = [-(np.pi + 1e-5), -2.8, -2.4, -2.0, -1.6, -1.2, -0.8, -0.4, 0.0, 0.4, 0.8, 1.2, 1.6, 2.0, 2.4, 2.8, (np.pi + 1e-5)]
ibu_bins["pT_ll"] = [200.0, 230.0, 300.0, 450.0, 600.0, 1000.0]
ibu_bins["y_ll"] = [-2.5, -2.0, -1.5, -1.0, -0.75, -0.5, -0.25, 0.0, 0.25, 0.5, 0.75, 1.0, 1.5, 2.0, 2.5]
ibu_bins["Ntracks_trackj1"] = [0.5, 6.5, 10.5, 14.5, 19.5, 25.5, 39.5]
ibu_bins["Ntracks_trackj2"] = [0.5, 3.5, 7.5, 11.5, 15.5, 34.5]
ibu_bins["m_trackj1"] = [0.0, 8.0, 16.0, 24.0, 32.0, 42.0, 70.0]
ibu_bins["m_trackj2"] = [0.0, 5.0, 10.0, 20.0, 40.0] 
ibu_bins["tau1_trackj1"] = [0.0, 0.05, 0.1, 0.17, 0.25, 0.35, 0.4, 0.9]
ibu_bins["tau1_trackj2"] = [0.0, 0.1, 0.2, 0.35, 0.5, 0.9]
ibu_bins["tau2_trackj1"] = [0.0, 0.025, 0.05, 0.08, 0.12, 0.17, 0.2, 0.5]
ibu_bins["tau2_trackj2"] = [0.0, 0.025, 0.1, 0.17, 0.25, 0.5]
ibu_bins["tau3_trackj1"] = [0.0, 0.025, 0.05, 0.1, 0.3]
ibu_bins["tau3_trackj2"] = [0.0, 0.025, 0.08, 0.14, 0.3]

plot_labels = {
    "pT_l1"           : r"Leading lepton $p_{\text{T}}$ [GeV]",
    "pT_l2"           : r"Subleading lepton $p_\text{T}$ [GeV]",
    "eta_l1"          : r"Leading lepton $\eta$",
    "eta_l2"          : r"Subleading lepton $\eta$",
    "phi_l1"          : r"Leading lepton $\phi$",
    "phi_l2"          : r"Subleading lepton $\phi$",
    "pT_trackj1"      : r"Leading jet $p_{\text{T}}$ [GeV]",
    "pT_trackj2"      : r"Subleading jet $p_{\text{T}}$ [GeV]",
    "y_trackj1"       : r"Leading jet $y$",
    "y_trackj2"       : r"Subleading jet $y$",
    "phi_trackj1"     : r"Leading jet $\phi$",
    "phi_trackj2"     : r"Subleading jet $\phi$",
    "pT_ll"           : r"Dilepton $p_{\text{T}}$ [GeV]",
    "y_ll"            : r"Dilepton $y$",
    "Ntracks_trackj1" : r"Leading jet $n_\text{ch}$ ",
    "Ntracks_trackj2" : r"Subleading jet $n_\text{ch}$ ",
    "m_trackj1"       : r"Leading jet $m$ [GeV]",
    "m_trackj2"       : r"Subleading jet $m$ [GeV]",
    "tau1_trackj1"    : r"Leading jet $\tau_1$",
    "tau1_trackj2"    : r"Subleading jet $\tau_1$",
    "tau2_trackj1"    : r"Leading jet $\tau_2$",
    "tau2_trackj2"    : r"Subleading jet $\tau_2$",
    "tau3_trackj1"    : r"Leading jet $\tau_3$",
    "tau3_trackj2"    : r"Subleading jet $\tau_3$",
}

labels = {
     'pT_l1' : r'd$\sigma$/d$p_\text{T}^{\mu 1}$ [fb/GeV]',
     'pT_l2' : r'd$\sigma$/d$p_\text{T}^{\mu 2}$ [fb/GeV]',
     'eta_l1' : r'd$\sigma$/d$\eta^{\mu 1}$ [fb]',
     'eta_l2' : r'd$\sigma$/d$\eta^{\mu 2}$ [fb]',
     'phi_l1' : r'd$\sigma$/d$\phi^{\mu 1}$ [fb]',
     'phi_l2' : r'd$\sigma$/d$\phi^{\mu 2}$ [fb]',
     'pT_trackj1' : r'd$\sigma$/d$p_\text{T}$ [fb/GeV]',
     'pT_trackj2' : r'd$\sigma$/d$p_\text{T}$ [fb/GeV]',
     'y_trackj1' : r'd$\sigma$/d$y$ [fb]',
     'y_trackj2' : r'd$\sigma$/d$y$ [fb]',
     'phi_trackj1' : r'd$\sigma$/d$\phi$ [fb]',
     'phi_trackj2' : r'd$\sigma$/d$\phi$ [fb]',
     'pT_ll' : r'd$\sigma$/d$p_\text{T}^{\mu\mu}$ [fb/GeV]',
     'y_ll' : r'd$\sigma$/d$y^{\mu\mu}$ [fb]',
     'Ntracks_trackj1' : r'd$\sigma$/dn$_\text{ch}$ [fb]',
     'Ntracks_trackj2' : r'd$\sigma$/dn$_\text{ch}$ [fb]',
     'm_trackj1' : r'd$\sigma$/d$m$ [fb/GeV]',
     'm_trackj2' : r'd$\sigma$/d$m$ [fb/GeV]',
     'tau1_trackj1' : r'd$\sigma$/d$\tau_1$ [fb]',
     'tau1_trackj2' : r'd$\sigma$/d$\tau_1$ [fb]',
     'tau2_trackj1' : r'd$\sigma$/d$\tau_2$ [fb]',
     'tau2_trackj2' : r'd$\sigma$/d$\tau_2$ [fb]',
     'tau3_trackj1' : r'd$\sigma$/d$\tau_3$ [fb]',
     'tau3_trackj2' : r'd$\sigma$/d$\tau_3$ [fb]',
 }

var_list = ['pT_l1','pT_l2','eta_l1','eta_l2','phi_l1','phi_l2','pT_trackj1','pT_trackj2','y_trackj1','y_trackj2','phi_trackj1','phi_trackj2','pT_ll','y_ll','Ntracks_trackj1','Ntracks_trackj2','m_trackj1','m_trackj2','tau1_trackj1','tau1_trackj2','tau2_trackj1','tau2_trackj2','tau3_trackj1','tau3_trackj2','tau21','dR_ll']




def draw_atlas_text(ax=None, 
                    simStatus='',
                    lines=[
                        r'$\sqrt{s} = $13 TeV, 139 fb$^{-1}$',
                        r'$Z\rightarrow\mu\mu$, $p_\text{T}^{\mu\mu} > 200$ GeV'
                    ],
                    linesize=12,
                    atlas_size=12,
                    xCoord = 0.04, lines_xCoord = .96, alignment='left', lines_alignment='right', shift0=0., shift1=0.22, shift =0.12, ytop=0.92, lines_ytop=1.0):
    if ax is None:
        ax = plt.gca()

    plt.rcParams.update({"text.usetex": True})

    plot_setup()
    ax.text(xCoord + shift0,
            ytop,
            # 'ATLAS',
            r"\textit{\textbf{\text{ATLAS}}}",
            transform=ax.transAxes,
            weight='bold',
            style='italic',
            size=atlas_size,
            ha=alignment,
            va='center',
            bbox=dict(facecolor='white', edgecolor='none', alpha=0.))
    ax.text(xCoord + shift1, # "Internal"
            ytop,
            simStatus,
            transform=ax.transAxes,
            size=atlas_size,
            ha=alignment,
            va='center',
            bbox=dict(facecolor='white', edgecolor='none', alpha=0.))
    for i, line in enumerate(lines):
        shift1 = 0.
        shift0 = -.47
        va = 'center'
        ax.text(lines_xCoord,
                lines_ytop - (i + 1) * shift,
                line,
                ha=lines_alignment,
                va=va,
                transform=ax.transAxes,
                size=linesize,
                bbox=dict(facecolor='none', edgecolor='none', alpha=0.))


        
### Define new variables
def calculate_vars(df): 
    ### tau21 = tau2/tau1 
    df['tau21'] = df.tau2_trackj1/df.tau1_trackj1
    
    ### deltaR(ll, j1) 
    def dR(v1, v2):
        dy = v1.rapidity-v2.rapidity
        dphi = v1.deltaphi(v2)
        return np.sqrt(dy**2 + dphi**2)

    def dR_ll_trackj1(pT_l1, eta_l1, phi_l1, 
                      pT_l2, eta_l2, phi_l2, 
                      pT_trackj1, y_trackj1, phi_trackj1):
        l1 = vector.array({
            "pt": pT_l1,
            "phi": phi_l1,
            "eta": eta_l1,
            "m":np.zeros_like(pT_l1)
        })
        l2 = vector.array({
            "pt": pT_l2,
            "phi": phi_l2,
            "eta": eta_l2,
            "m":np.zeros_like(pT_l2)
        })
        track_j1 = vector.array({
            "pt": pT_trackj1,
            "phi": phi_trackj1,
            "eta": y_trackj1,
            "m":np.zeros_like(pT_trackj1)
        })

        ll = l1 + l2
        return dR(ll, track_j1)
    
    def phi_ll(pt_l1, eta_l1, phi_l1, pt_l2, eta_l2, phi_l2): 
            l1 = vector.array({"pt": pt_l1, "eta": eta_l1, "phi": phi_l1, "m": np.zeros(len(pt_l1))})
            l2 = vector.array({"pt": pt_l2, "eta": eta_l2, "phi": phi_l2, "m": np.zeros(len(pt_l2))})
            return l1.add(l2).phi
    
    df['phi_ll'] = phi_ll(np.array(df.pT_l1), np.array(df.eta_l1), np.array(df.phi_l1), 
                          np.array(df.pT_l2), np.array(df.eta_l2), np.array(df.phi_l2))
    df['dR_ll'] = dR_ll_trackj1(
        np.array(df.pT_l1), np.array(df.eta_l1), np.array(df.phi_l1), 
        np.array(df.pT_l2), np.array(df.eta_l2), np.array(df.phi_l2),
        np.array(df.pT_trackj1), np.array(df.y_trackj1), np.array(df.phi_trackj1))
    
    return df


def corr_matrix(var, bins_dict, multifold, multifold_sherpa, multifold_nonDY, smooth=True):
    bins = np.array(bins_dict[var])
    bin_centers = 0.5 * (bins[1:] + bins[:-1])
    bin_widths = [bins[n+1] - bins[n] for n in range(len(bins)-1)]
    
    ### Construct masks for track pT cuts
    mask_trackj1 = np.array(multifold['pT_trackj1'] > 5, dtype='bool')
    mask_trackj2 = np.array(multifold['pT_trackj2'] > 5, dtype='bool')

    mask_sherpa_trackj1 = np.array(multifold_sherpa['pT_trackj1'] > 5, dtype='bool')
    mask_sherpa_trackj2 = np.array(multifold_sherpa['pT_trackj2'] > 5, dtype='bool')

    mask_nonDY_trackj1 = np.array(multifold_nonDY['pT_trackj1'] > 5, dtype='bool')
    mask_nonDY_trackj2 = np.array(multifold_nonDY['pT_trackj2'] > 5, dtype='bool')

    if "trackj1" in var: 
        df = multifold[mask_trackj1]
        df_sherpa = multifold_sherpa[mask_sherpa_trackj1]
        df_nonDY = multifold_nonDY[mask_nonDY_trackj1]
    elif "trackj2" in var:
        df = multifold[mask_trackj2]
        df_sherpa = multifold_sherpa[mask_sherpa_trackj2]
        df_nonDY = multifold_nonDY[mask_nonDY_trackj2]
    else:
        df = multifold
        df_sherpa = multifold_sherpa
        df_nonDY = multifold_nonDY
        
    event_systs     = [col for col in multifold.keys() if col.startswith("weights_muEff")] + ['weights_pileup']
    theory_systs    = [col for col in multifold.keys() if col.startswith("weights_theory")]
    track_systs     = [col for col in multifold.keys() if col.startswith("weights_track")]
    muon_systs      = [col for col in multifold.keys() if col.startswith("weights_muCal")]
    mc_stat_systs   = [col for col in multifold.keys() if col.startswith("weights_bootstrap_mc")]
    data_stat_systs = [col for col in multifold.keys() if col.startswith("weights_bootstrap_data")]
    ensemble_systs  = [col for col in multifold.keys() if col.startswith("weights_ensemble")]
    
    ### MultiFold
    nom, _         = np.histogram(df[var], bins=bins, weights=df.weights_nominal)    
    mc_stat_cov, _ = np.histogram(df[var], bins=bins, weights=df.weights_nominal**2)

    ### MC Stat
    mc_stat_bs = []
    for syst_name in mc_stat_systs:
        syst, _ = np.histogram(df[var], bins=bins, weights=df[syst_name])
        mc_stat_bs.append(syst)
    mc_bs = np.mean(mc_stat_bs, axis=0)

    ### Data Stat
    data_stat_bs = []
    for syst_name in data_stat_systs:
        syst, _ = np.histogram(df[var], bins=bins, weights=df[syst_name])
        data_stat_bs.append(syst)

    ### NN Initialization
    ensembles = []
    for syst_name in ensemble_systs:
        syst, _ = np.histogram(df[var], bins=bins, weights=df[syst_name])
        ensembles.append(syst)
    nn_init = np.mean(ensembles, axis=0)

    theory_bin_uncerts = []
    unfolding_dd_bin_uncerts = []
    unfolding_hv_bin_uncerts = []
    bkg_bin_uncerts = []
    lumi_bin_uncerts = []

    ### Theory 
    for syst_name in theory_systs:
        syst, _ = np.histogram(df[var], bins=bins,  weights=df[syst_name])
        theory_bin_uncerts.append(syst - nom)
    
    ### Unfolding (DD)
    counts_dd, _ = np.histogram(df[var], bins=bins, weights=df.weights_dd)
    target_dd, _ = np.histogram(df[var], bins=bins, weights=df.target_dd)
    unfolding_dd_bin_uncerts.append((counts_dd - target_dd)*(nom/target_dd))
    
    ### Unfolding (HV)
    sherpa_counts, _ = np.histogram(df_sherpa[var], bins=bins, weights=df_sherpa.weights_nominal)
    if smooth: 
        unfolding_hv_bin_uncerts.append(smooth_uncertainty(sherpa_counts - nom, bin_centers))
    else:
        unfolding_hv_bin_uncerts.append(sherpa_counts - nom)
        
    ### Top Background 
    syst, _ = np.histogram(df[var], bins=bins, weights=df.weights_topBackground)
    bkg_bin_uncerts.append(syst - nom)

    ### Non-Strong Background
    syst, _ = np.histogram(df_nonDY[var], bins=bins, weights=df_nonDY.weights_nominal)
    bkg_bin_uncerts.append(syst - nom)   
    
    ### Luminosity 
    syst, _ = np.histogram(df[var], bins=bins, weights=df.weights_lumi)
    lumi_bin_uncerts.append(syst-nom)

    ### CALCULATE COVARIANCE MATRIX
    n = len(bin_widths)
    n_bootstraps = 25

    ### MC stat covariance per bin
    v_mc = np.zeros((n, n), float)
    np.fill_diagonal(v_mc, mc_stat_cov) # only nonzero on the diagonal

    ### MC & Data Bootstrap uncertainty 
    v_bs_mc = fill_cov_matrix(n, mc_stat_bs, np.mean(mc_stat_bs, axis=0))
    v_bs_data = fill_cov_matrix(n, data_stat_bs, np.mean(data_stat_bs, axis=0))

    ### NN initialization uncertainty 
    v_nn = fill_cov_matrix(n, ensembles, np.mean(ensembles, axis=0))/100

    ### Other uncertainties
    v_theory = fill_cov_matrix(n, theory_bin_uncerts)
    v_bkg = fill_cov_matrix(n, bkg_bin_uncerts)
    v_unfolding_dd = fill_cov_matrix(n, unfolding_dd_bin_uncerts)
    v_unfolding_hv = fill_cov_matrix(n, unfolding_hv_bin_uncerts)
    v_lumi = fill_cov_matrix(n, lumi_bin_uncerts)

    ### Construct covariance matrix
    v_total = v_mc + v_bs_mc + v_bs_data + v_nn + v_theory + v_unfolding_dd + v_lumi  + v_unfolding_hv + v_bkg
    
    sigmas = np.sqrt(np.diag(v_total))
    corr_matrix = np.zeros((len(sigmas), len(sigmas)))
    
    for i in range(len(bin_widths)):
        for j in range(len(bin_widths)):
            corr_matrix[i,j] = v_total[i,j]/(sigmas[i]*sigmas[j])

    return corr_matrix         

def plot(df, df_sherpa, df_nonDY, mc_preds, var, labels, plot_labels, bins, title=None, std=None): 
    uncertainties = {}
    
    event_systs     = [col for col in df.keys() if col.startswith("weights_muEff")] + ['weights_pileup']
    theory_systs    = [col for col in df.keys() if col.startswith("weights_theory")]
    track_systs     = [col for col in df.keys() if col.startswith("weights_track")]
    muon_systs      = [col for col in df.keys() if col.startswith("weights_muCal")]
    mc_stat_systs   = [col for col in df.keys() if col.startswith("weights_bootstrap_mc")]
    data_stat_systs = [col for col in df.keys() if col.startswith("weights_bootstrap_data")]
    ensemble_systs  = [col for col in df.keys() if col.startswith("weights_ensemble")]

    nom, _ = np.histogram(df[var], bins=bins, weights=df.weights_nominal)    
    final_event          = calculate_uncertainty(df, var, bins, event_systs)
    final_theory         = calculate_uncertainty(df, var, bins, theory_systs)
    final_track          = calculate_uncertainty(df, var, bins, track_systs)
    final_muon           = calculate_uncertainty(df, var, bins, muon_systs)
    final_mc_stat        = calculate_stat_uncertainty(df, var, bins, mc_stat_systs)
    final_data_stat      = calculate_stat_uncertainty(df, var, bins, data_stat_systs)
    final_nn_init        = calculate_stat_uncertainty(df, var, bins, ensemble_systs)/np.sqrt(len(ensemble_systs))

    ### Unfolding (DD)
    syst, _ = np.histogram(df[var], bins=bins, weights=df.weights_dd)
    target, _ = np.histogram(df[var], bins=bins, weights=df.target_dd)
    final_unfolding_dd = 100*np.sqrt((syst-target)**2)/target

    ### Unfolding (HV)
    syst, _ = np.histogram(df_sherpa[var], bins=bins, weights=df_sherpa.weights_nominal)
    final_unfolding_hv = 100*np.sqrt((syst-nom)**2)/nom

    ### Sum these together in quadrature
    final_unfolding = np.sqrt(final_unfolding_hv**2 + final_unfolding_dd**2)

    ### Luminosity 
    syst, _ = np.histogram(df[var], bins=bins, weights=df.weights_lumi)
    final_lumi = 100*np.sqrt((syst-nom)**2)/nom

    ### Top Background 
    syst, _ = np.histogram(df[var], bins=bins, weights=df.weights_topBackground)
    final_top = 100*np.sqrt((syst-nom)**2)/nom

    ### Non-Strong Background
    syst, _ = np.histogram(df_nonDY[var], bins=bins, weights=df_nonDY.weights_nominal)
    final_nonDY = 100*np.sqrt((syst-nom)**2)/nom

    uncertainties[var+"_lumi"] = final_lumi
    uncertainties[var+"_theory"] = final_theory
    uncertainties[var+"_event"] = final_event
    uncertainties[var+"_track"] = final_track
    uncertainties[var+"_muon"] = final_muon
    uncertainties[var+"_mc_stat"] = final_mc_stat
    uncertainties[var+"_data_stat"] = final_data_stat
    uncertainties[var+"_nn_init"] = final_nn_init
    uncertainties[var+"_unfolding"] = final_unfolding
    uncertainties[var+"_top"] = final_top
    uncertainties[var+"_nonDY"] = final_nonDY

    total = np.sqrt(
          final_lumi**2
        + final_theory**2 
        + final_event**2
        + final_track**2
        + final_muon**2
        + final_mc_stat**2
        + final_data_stat**2
        + final_nn_init**2
        + final_unfolding**2
        + final_top**2
        + final_nonDY**2
    )

    uncertainties[var+"_total"] = total
        
    fig = plt.figure(figsize=(4*2,3), dpi=300, constrained_layout=True)
    subfigs = fig.subfigures(1, 2)

    for j, subfig in enumerate(subfigs.flat):
        if j == 0: # left figure 
            axs = subfig.subplots(2, 1, sharex=True, sharey=False, gridspec_kw = {'height_ratios':[3, 1]})
            bin_centers = 0.5 * (bins[1:] + bins[:-1])
            bin_widths = np.array([bins[n+1] - bins[n] for n in range(len(bins)-1)])

            if var in var_list:
                i = var_list.index(var)
                ### Sherpa
                counts = mc_preds[i]['sherpa_counts']
                sherpa_density = counts/lumi/bin_widths
                sherpa_error = mc_preds[i]['sherpa_err']/mc_preds[i]['sherpa_counts']
                _ = make_error_boxes(axs[0], bin_centers, sherpa_density, np.vstack([bin_widths/2,bin_widths/2]), np.vstack([sherpa_density*sherpa_error, sherpa_density*sherpa_error]), facecolor='deeppink', alpha=0.25, marker="s", label=r'Drell-Yan: Sherpa 2.2.11 + $\mathbf{X}$'+'\n'+r'($\mathbf{X} =$ EW $Zjj$, $VZ\to V\mu\mu$)')

                ### MGFxFx 
                counts = mc_preds[i]['mgfxfx_counts']
                mgfxfx_density = counts/lumi/bin_widths
                mgfxfx_error = mc_preds[i]['mgfxfx_err']/mc_preds[i]['mgfxfx_counts']
                _ = make_error_boxes(axs[0], bin_centers, mgfxfx_density, np.vstack([bin_widths/2,bin_widths/2]), np.vstack([mgfxfx_density*mgfxfx_error, mgfxfx_density*mgfxfx_error]), facecolor='dodgerblue', alpha=0.25, marker="^", label=r'Drell-Yan: MG5+Py8 + $\mathbf{X}$')

            ### MultiFold
            multifold_density, _, _ = axs[0].hist(df[var], weights=df.weights_nominal, bins=bins, color='black', linewidth= 2, density=True, alpha=0)
            multifold_density *= np.sum(df.weights_nominal)

            axs[0].minorticks_on()
            axs[1].minorticks_on()
            axs[0].xaxis.set_tick_params(labelsize=12, which='both', direction='in', top=True)
            axs[0].yaxis.set_tick_params(labelsize=12, which='both', direction='in', right=True)
            axs[0].set_ylabel(labels[var], fontsize=16, labelpad=2, loc='top')
            axs[0].set_ylim([0, 1.7 * max(multifold_density)])
            if std is not None: 
                axs[0].text(0.6, 3000, std, 
                            # ha="center", va="center", 
                            color="black", fontsize=12, fontweight='bold')

            lines=[
                r'$\sqrt{s} = $13 TeV, 139 fb$^{-1}$',
                r'$Z\rightarrow\mu\mu$, $p_T^{\mu\mu} > 200$ GeV'
            ]
            if 'trackj1' in var:
                lines.append(r'Anti-$k_t$ $R=0.4$, $p_\mathrm{T}^{j1}>5\,\mathrm{GeV}$')
            elif 'trackj2' in var:
                lines.append(r'Anti-$k_t$ $R=0.4$, $p_\mathrm{T}^{j2}>5\,\mathrm{GeV}$')

            draw_atlas_text(ax=axs[0], lines=lines) 
            axs[0].errorbar(bin_centers, multifold_density, xerr=bin_widths/2, yerr=multifold_density*uncertainties[var+"_total"]/100, marker=".", linestyle="None", color="k", alpha=1, ecolor='k', label=r"$\textsc{OmniFold}$ Measurement", markersize=4, linewidth=1)
            axs[0].legend(fontsize=9, frameon=False, loc='upper left', bbox_to_anchor=(0.3, 0.75), bbox_transform=axs[0].transAxes); # loc='best'
            axs[0].set_title(title)

            axs[1].errorbar(bin_centers, np.ones(len(bin_centers)), xerr=bin_widths/2, yerr=uncertainties[var+"_total"]/100, marker=".", linestyle="None", color="k", alpha=1,  ecolor='k', label="OmniFold Data", markersize=4, linewidth=1)
            if var in var_list:
                _ = make_error_boxes(axs[1], bin_centers, sherpa_density/multifold_density, np.vstack([bin_widths/2,bin_widths/2]), np.vstack([(sherpa_density/multifold_density)*sherpa_error, (sherpa_density/multifold_density)*sherpa_error]), facecolor='deeppink', alpha=0.25, marker="s", label=r'Sherpa 2.2.11 + $\mathbf{X}$')
                _ = make_error_boxes(axs[1], bin_centers, mgfxfx_density/multifold_density, np.vstack([bin_widths/2,bin_widths/2]), np.vstack([(mgfxfx_density/multifold_density)*mgfxfx_error, (mgfxfx_density/multifold_density)*mgfxfx_error]), facecolor='dodgerblue', alpha=0.25, marker="^", label=r'MG5+Py8 + $\mathbf{X}$')
            axs[1].set_xlim(bins[0], bins[-1])
            axs[1].set_ylim([0.2,1.8])
            axs[1].xaxis.set_tick_params(labelsize=12, which='both', direction='in', top=True)
            axs[1].yaxis.set_tick_params(labelsize=12, which='both', direction='in', right=True)
            axs[1].set_ylabel('Ratio\nto Data', fontsize=12, labelpad=6, loc='center')
            axs[1].set_xlabel(plot_labels[var], fontsize=16, labelpad=2, loc='right');

        if j == 1: # right figure 
            ax = subfig.subplots(1, 1)
            colors = [plt.cm.Paired(i) for i in range(20)]
            colors[9] = colors[8]
            colors[8] = 'mediumvioletred'
            colors[10] = 'goldenrod'
            colors[11] = 'k'
            bin_centers = 0.5 * (bins[1:] + bins[:-1])

            ax.step(bin_centers, uncertainties[var+'_lumi'],           where="mid",  label = "Luminosity",     color=colors[0],    )
            ax.step(bin_centers, uncertainties[var+'_theory'],         where="mid",  label = "Theory",         color=colors[1],    )
            ax.step(bin_centers, uncertainties[var+'_event'],          where="mid",  label = "Event",          color=colors[2],    )
            ax.step(bin_centers, uncertainties[var+'_track'],          where="mid",  label = "Track",          color=colors[3],    )
            ax.step(bin_centers, uncertainties[var+'_muon'],           where="mid",  label = "Muon",           color=colors[4],    )
            ax.step(bin_centers, uncertainties[var+'_mc_stat'],        where="mid",  label = "MC Stat",        color=colors[5],    )
            ax.step(bin_centers, uncertainties[var+'_data_stat'],      where="mid",  label = "Data Stat",      color=colors[6],    )
            ax.step(bin_centers, uncertainties[var+'_nn_init'],        where="mid",  label = "NN Init.",       color=colors[7],    )
            ax.step(bin_centers, uncertainties[var+'_unfolding'],      where="mid",  label = "Unfolding",      color=colors[8])
            ax.step(bin_centers, uncertainties[var+'_top'],            where="mid",  label = "Top",            color=colors[9],    )
            ax.step(bin_centers, uncertainties[var+'_nonDY'],          where="mid",  label = "Non-Strong",     color=colors[10],     )
            ax.step(bin_centers, uncertainties[var+'_total'],          where="mid",  label = "Total",          color=colors[11],         )

            ### draw lines at the beginning and end of the first and last bins, for plotting purposes
            label_list = ['lumi', 'theory', 'event', 'track', 'muon', 'mc_stat', 'data_stat', 'nn_init', 'unfolding', 'top', 'nonDY', 'total']
            for label in label_list:
                ax.hlines(y=uncertainties[var+'_'+label][0], xmin=bins[0], xmax=bin_centers[0], color=colors[label_list.index(label)])
                ax.hlines(y=uncertainties[var+'_'+label][-1], xmin=bin_centers[-1], xmax=bins[-1], color=colors[label_list.index(label)])

            lines=[
                r'$\sqrt{s} = $13 TeV, 139 fb$^{-1}$',
                r'$Z\rightarrow\mu\mu$+jets, $p_T^{\mu\mu} > 200$ GeV'
            ]
            if var == 'tau21' or var == 'dR_ll':
                lines[-1] = r'$Z\rightarrow\mu\mu$+jets, $p_T^{\mu\mu} > 200$ GeV, $p_T^{j_1} > 5$ GeV'

            draw_atlas_text(ax=ax, lines=lines) 
            ax.xaxis.set_tick_params(labelsize=12)
            ax.yaxis.set_tick_params(labelsize=12)
            ax.set_ylabel(r"Fractional Systematic Uncertainty [\%]", fontsize=9, labelpad=2, loc='top')
            ax.legend(fontsize=8, loc='center left', ncol=2, frameon=False)
            ax.set_ylim([0, 25])
            ax.set_xlabel(plot_labels[var], fontsize=12, labelpad=2, loc='right');
            ax.set_xlim([bins[0], bins[-1]])
            
    return multifold_density, total, uncertainties


# Gaussian Kernel smoothing
# Does not consider the 'uncertainty on the uncertainty' 
from scipy.stats import norm
def smooth_uncertainty(uncert,bin_centers):
    # Parameter for smoothing
    # The Gaussian Kernel width will be 'full-range'/Nsig
    Nsig = 10
    xrange = (bin_centers[-1]-bin_centers[0])
    logScale = bin_centers[0] > 0
    if logScale:
        xrange = (np.log(bin_centers[-1])-np.log(bin_centers[0]))
    kernel_width = xrange/Nsig

    # will hold the uncertainty
    smooth = uncert.copy()
    for bin_i in range(0,len(bin_centers)):
        x_i = np.log(bin_centers[bin_i]) if logScale else bin_centers[bin_i]
        sumw = sumwy = 0
        for bin_j in range(0,len(bin_centers)):
            x_j = np.log(bin_centers[bin_j]) if logScale else bin_centers[bin_j]
            # Kernel weight
            w = norm.pdf((x_j-x_i)/kernel_width)
            sumw += w
            sumwy += w*uncert[bin_j]

        smooth[bin_i] = sumwy/sumw
    return smooth

def plot_single(df, df_sherpa, df_nonDY, mc_preds, var, labels, plot_labels, bins, legend_loc='lower right', lines_alignment='right', lines_xCoord=0.96, title=None, std=None, filename=None): 
    uncertainties = {}

    event_systs     = [col for col in df.keys() if col.startswith("weights_muEff")] + ['weights_pileup']
    theory_systs    = [col for col in df.keys() if col.startswith("weights_theory")]
    track_systs     = [col for col in df.keys() if col.startswith("weights_track")]
    muon_systs      = [col for col in df.keys() if col.startswith("weights_muCal")]
    mc_stat_systs   = [col for col in df.keys() if col.startswith("weights_bootstrap_mc")]
    data_stat_systs = [col for col in df.keys() if col.startswith("weights_bootstrap_data")]
    ensemble_systs  = [col for col in df.keys() if col.startswith("weights_ensemble")]

    nom, _ = np.histogram(df[var], bins=bins, weights=df.weights_nominal)    
    final_event          = calculate_uncertainty(df, var, bins, event_systs)
    final_theory         = calculate_uncertainty(df, var, bins, theory_systs)
    final_track          = calculate_uncertainty(df, var, bins, track_systs)
    final_muon           = calculate_uncertainty(df, var, bins, muon_systs)
    final_mc_stat        = calculate_stat_uncertainty(df, var, bins, mc_stat_systs)
    final_data_stat      = calculate_stat_uncertainty(df, var, bins, data_stat_systs)
    final_nn_init        = calculate_stat_uncertainty(df, var, bins, ensemble_systs)/np.sqrt(len(ensemble_systs))

    ### Unfolding (DD)
    syst, _ = np.histogram(df[var], bins=bins, weights=df.weights_dd)
    target, _ = np.histogram(df[var], bins=bins, weights=df.target_dd)
    final_unfolding_dd = 100*np.sqrt((syst-target)**2)/target

    ### Unfolding (HV)
    syst, _ = np.histogram(df_sherpa[var], bins=bins, weights=df_sherpa.weights_nominal)
    final_unfolding_hv = 100*np.sqrt((syst-nom)**2)/nom

    ### Sum these together in quadrature
    final_unfolding = np.sqrt(final_unfolding_hv**2 + final_unfolding_dd**2)

    ### Luminosity 
    syst, _ = np.histogram(df[var], bins=bins, weights=df.weights_lumi)
    final_lumi = 100*np.sqrt((syst-nom)**2)/nom

    ### Top Background 
    syst, _ = np.histogram(df[var], bins=bins, weights=df.weights_topBackground)
    final_top = 100*np.sqrt((syst-nom)**2)/nom

    ### Non-Strong Background
    syst, _ = np.histogram(df_nonDY[var], bins=bins, weights=df_nonDY.weights_nominal)
    final_nonDY = 100*np.sqrt((syst-nom)**2)/nom

    uncertainties[var+"_lumi"] = final_lumi
    uncertainties[var+"_theory"] = final_theory
    uncertainties[var+"_event"] = final_event
    uncertainties[var+"_track"] = final_track
    uncertainties[var+"_muon"] = final_muon
    uncertainties[var+"_mc_stat"] = final_mc_stat
    uncertainties[var+"_data_stat"] = final_data_stat
    uncertainties[var+"_nn_init"] = final_nn_init
    uncertainties[var+"_unfolding"] = final_unfolding
    uncertainties[var+"_top"] = final_top
    uncertainties[var+"_nonDY"] = final_nonDY

    total = np.sqrt(
          final_lumi**2
        + final_theory**2 
        + final_event**2
        + final_track**2
        + final_muon**2
        + final_mc_stat**2
        + final_data_stat**2
        + final_nn_init**2
        + final_unfolding**2
        + final_top**2
        + final_nonDY**2
    )

    uncertainties[var+"_total"] = total

    fig = plt.figure(figsize=(4,3), dpi=300, constrained_layout=True)

    axs = fig.subplots(2, 1, sharex=True, sharey=False, gridspec_kw = {'height_ratios':[3, 1]})
    bin_centers = 0.5 * (bins[1:] + bins[:-1])
    bin_widths = np.array([bins[n+1] - bins[n] for n in range(len(bins)-1)])

    ### MGFxFx 
    mgfxfx_density, _, _ = axs[0].hist(df[var], weights=df.weight_mc, bins=bins, color='blue', linewidth= 2, density=True, alpha=0)
    mgfxfx_density *= np.sum(df.weight_mc)
    mgfxfx_error = uncertainties[var+"_mc_stat"]/100
    
    _ = make_error_boxes(axs[0], bin_centers, mgfxfx_density, np.vstack([bin_widths/2,bin_widths/2]), np.vstack([mgfxfx_density*mgfxfx_error, mgfxfx_density*mgfxfx_error]), facecolor='dodgerblue', alpha=1, marker="^", label=r'Drell-Yan: MG5+Py8')

    ### MultiFold
    multifold_density, _, _ = axs[0].hist(df[var], weights=df.weights_nominal, bins=bins, color='black', linewidth= 2, density=True, alpha=0)
    multifold_density *= np.sum(df.weights_nominal)

    axs[0].minorticks_on()
    axs[1].minorticks_on()
    axs[0].xaxis.set_tick_params(labelsize=12, which='both', direction='in', top=True)
    axs[0].yaxis.set_tick_params(labelsize=12, which='both', direction='in', right=True)
    axs[0].set_ylabel(labels[var], fontsize=16, labelpad=2, loc='top')
    axs[0].set_ylim([1, 100000])
#axs[0].set_ylim([0, 1.7 * max(multifold_density)])
    axs[0].set_yscale('log')

    lines=[
        r'$\sqrt{s} = $13 TeV, 139 fb$^{-1}$',
        r'$Z\rightarrow\mu\mu$, $p_\text{T}^{\mu\mu} > 200$ GeV'
    ]
    if 'trackj1' in var:
        lines.append(r'Anti-$k_t$ $R=0.4$, $p_\mathrm{T}^{j1}>5\,\mathrm{GeV}$')
    elif 'trackj2' in var:
        lines.append(r'Anti-$k_t$ $R=0.4$, $p_\mathrm{T}^{j2}>5\,\mathrm{GeV}$')

    draw_atlas_text(ax=axs[0], lines=lines, linesize=9, lines_ytop=0.9,  lines_xCoord = lines_xCoord, lines_alignment=lines_alignment) 
    axs[0].errorbar(bin_centers, multifold_density, xerr=bin_widths/2, yerr=multifold_density*uncertainties[var+"_total"]/100, marker=".", linestyle="None", color="k", alpha=1, ecolor='k', label=r"$\textsc{OmniFold}$ Measurement", markersize=4, linewidth=1)
    axs[0].legend(fontsize=9, frameon=False, loc=legend_loc);
    axs[0].set_title(title)

    axs[1].errorbar(bin_centers, np.ones(len(bin_centers)), xerr=bin_widths/2, yerr=uncertainties[var+"_total"]/100, marker=".", linestyle="None", color="k", alpha=1,  ecolor='k', label="OmniFold Data", markersize=4, linewidth=1)
    _ = make_error_boxes(axs[1], bin_centers, mgfxfx_density/multifold_density, np.vstack([bin_widths/2,bin_widths/2]), np.vstack([(mgfxfx_density/multifold_density)*mgfxfx_error, (mgfxfx_density/multifold_density)*mgfxfx_error]), facecolor='dodgerblue', alpha=1, marker="^", label=r'MG5+Py8 + $\mathbf{X}$')
    axs[1].set_xlim(bins[0], bins[-1])
    axs[1].set_ylim([0.2,1.8])
    axs[1].xaxis.set_tick_params(labelsize=12, which='both', direction='in', top=True)
    axs[1].yaxis.set_tick_params(labelsize=12, which='both', direction='in', right=True)
    axs[1].set_ylabel('MC/Data', fontsize=12, labelpad=6, loc='center')
    axs[1].set_xlabel(plot_labels[var], fontsize=16, labelpad=2, loc='right');
    
    plt.savefig("./plots/data/"+filename)
    
    return multifold_density, total, uncertainties


from matplotlib.collections import PatchCollection
from matplotlib.patches import Rectangle

def make_error_boxes(ax, xdata, ydata, xerror, yerror, facecolor='r',
                     edgecolor='none', alpha=0.4, label=None, marker='.', fillstyle='full', markersize=3):

    # Loop over data points; create box from errors at each point
    errorboxes = [Rectangle((x - xe[0], y - ye[0]), xe.sum(), ye.sum())
                  for x, y, xe, ye in zip(xdata, ydata, xerror.T, yerror.T)]

    # Create patch collection with specified colour/alpha
    pc = PatchCollection(errorboxes, facecolor=facecolor, alpha=alpha,
                         edgecolor=edgecolor)

    # Add collection to axes
    ax.add_collection(pc)

    # Plot errorbars
    artists = ax.errorbar(xdata, ydata, xerr=xerror, yerr=yerror, linestyle='None', linewidth=0,
                            label=label, marker=marker, color=facecolor, fillstyle='none', markersize=markersize)

    return artists
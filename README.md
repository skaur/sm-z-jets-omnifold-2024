# A simultaneous unbinned differential cross section measurement of twenty-four $Z$+jets kinematic observables with the ATLAS detector

[![CDS - CERN-EP-2024-132](https://img.shields.io/badge/CDS-CERN--EP--2024--132-4D94CE)](https://cds.cern.ch/record/2899105)
[![arXiv](https://img.shields.io/badge/arXiv-2405.20041-b31b1b.svg?style=flat)](https://arxiv.org/abs/2405.20041)
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.11507450.svg)](https://zenodo.org/records/11507450)

These notebooks demonstrate how to interact with the unbinned, twenty-four-dimensional ATLAS $Z$+jets differential cross-section measurement presented in [arXiv:2405.20041](https://arxiv.org/abs/2405.20041). This analysis uses [OmniFold](https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.124.182001) to mitigate detector effects in data.
![OmniFold diagram.](./omnifold_figure.png)

Since the data is structured as an unbinned set of events, users can:
- Re-create the differential cross-section distributions (and calculate the associated uncertainties) of the twenty-four measured input observables with a **flexible choice of binnings** (see Fig. 1 below) 
- **Modify the measured phase space** on-the-fly (see Fig. 2a & 2b below)
- **Measure new observables** or quantities constructed as a function of the input observables (see Fig. 2 below)

![Example plots of the nominal measurement.](./fig1.png)
![Example plots of derived variables constructed as a function of the inputs.](./fig2.png)

## Repository structure

- `1_basics.ipynb`: Run this notebook first to learn more about the measurement format and how to obtain important quantities such as the measured cross-section and associated uncertainties. 
- `2_pseudo_results.ipynb`: Run this notebook next to interact with the unfolded "pseudo-data", compare it with the known truth-level target quantities, and calculate $\chi^2$ compatibility tests. 
- `3_results.ipynb`: Finally, run this notebook to interact with the unfolded data and plot the differential cross-sections. 

## Measured quantities
The signal process is inclusive $Z\to\mu\mu$ production with a fiducial region defined in the boosted regime: $p_\text{T}^{\mu\mu}$ > 200 GeV, and in total, 24 $Z$+jets kinematic observables are measured. The 24 observables are:
- $p_\text{T}$, $\eta$ and $\phi$ of each of the two muons (6 observables)
- The $p_\text{T}$ and rapidity of the dimuon system: $p_\text{T}^{\mu\mu}$, $y_{\mu\mu}$ (2 observables)
- The 4-momenta $(p_\text{T},y,\phi,m)$ of the two leading charged particle jets (8 observables)
- The number of (charged) constituents and $n$-subjettiness quantities $\tau_1$, $\tau_2$ and $\tau_3$ for each of the same two jets (8 observables)

The dimuon system $p_\text{T}$ and $y$ can be obtained from the muon kinematics, but they are included for convenience. The observables are labeled by 1 and 2 for leading and subleading in $p_\text{T}$, respectively.

The results are published as Pandas DataFrames with a format that looks like this: 
| Event # | pT_l1      | pT_l2      | eta_l1     | eta_l2     | ...  | weights_theoryQCD | weights_theoryPDF | weights_topBackground | weights_lumi |
|---------|------------|------------|------------|------------|------|-------------------|-------------------|-----------------------|--------------|
| 0       | 288.466919 | 198.183929 | -0.117443  | -0.334414  | ...  | 0.003152          | 0.003348          | 0.003240              | 0.003423     |
| 1       | 166.120789 | 125.378044 | 0.313321   | 0.368928   | ...  | 0.008651          | 0.003967          | 0.008191              | 0.008061     |
| 2       | 335.697479 | 133.157684 | 0.766387   | 0.534180   | ...  | 0.001614          | 0.002421          | 0.001746              | 0.001806     |
| 3       | 189.518021 | 25.711994  | 1.083798   | -0.190828  | ...  | 0.004376          | 0.006329          | 0.004604              | 0.004761     |

Note that in addition to the 24 measured observables, there are also a number of event weights. The event weight associated with the nominal measurement is `weights_nominal`; other columns starting with `weights_*` correspond to uncertainties. 

## Requirements 
The required packages to run these notebooks are:
- jupyter
- ipykernel
- numpy 
- pandas
- matplotlib
- tqdm
- vector
- tables
- scipy

Note that these are all `pip`-installable:
```sh
pip install jupyter ipykernel numpy pandas matplotlib tqdm vector tables scipy
```

## Quick Start Guide

### Google Colab
- `1_basics.ipynb`: [![Open in Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/drive/1L13XPeB6peLiWcO9CvAM7l8GfkbkPAxS)
- `2_pseudo_results.ipynb`: [![Open in Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/drive/1L0ifnkGhONl85_rAzVakfIMlBi2FitoM)
- `3_results.ipynb`: [![Open in Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/drive/1KzrjP8tvIRCVDieNngKiNa1yDwuGaU56)

### Local use
First, clone this repository to your local machine: 
```sh
git clone ssh://git@gitlab.cern.ch:7999/atlas-physics/sm/wz/z-jets-omnifold-2024.git
```

Next, install the required software packages. We recommend using a virtual environment such as `venv` or `conda`: 
```sh
conda env create -f env.yml
```
OR

```sh
conda create -n env python=3
conda activate env
pip install jupyter ipykernel numpy pandas matplotlib tqdm vector tables scipy
```

Then, install a Jupyter kernel, so that these packages are visible to your Jupyter notebook
```sh
python -m ipykernel install --user --name env --display-name "env"
```
Restart your Jupyter session so that you can select this kernel for your notebook.

Then, download the data files to the `files` folder: 
- [Public link to download data files](https://zenodo.org/records/11507450)

### SWAN
For CERN users, these notebooks will run out-of-the-box on [SWAN](https://swan.cern.ch/). 
1. Start a SWAN session. Note that you will need to request a memory allocation of **at least 10 GB**. 
2. Under "My Projects", in the top right corner, click the icon that looks like a cloud with an arrow pointing down ("Download Project from git") and paste the repository URL + ".git": 
```https://gitlab.cern.ch/atlas-physics/sm/wz/z-jets-omnifold-2024.git```
3. Download the data files to the `files` folder:
    - [Public link to download data files](https://zenodo.org/records/11507450)

## Usage recommendations
As detailed in the paper, the unbinned measurements were validated both in the full phase space ($p_T^{\mu\mu}$>200 GeV) and in various kinematic sub-regions. In these regions, a series of binned differential cross sections were studied, both for the 24 directly measured observables and additional observables derived as a function of the 24, such as $m_{jj}$, $\Delta y_{jj}$ and $\Delta R(\ell\ell,j_1)$. 

When using these results, the following guidance is recommended:

1. **Jet $p_\text{T}$ threshold:** When studying jet observables, a jet $p_\text{T}$ threshold should be applied of at least 5 GeV. For example, if plotting $\Delta R(\ell\ell,j_1)$, which relies on the leading jet, a requirement should be placed on the leading jet $p_\text{T}$, and for $m_{jj}$, $p_\text{T}$ requirements should be applied to both jets.
2. **Sufficient Monte Carlo statistical precision:** Each bin should contain a minimum of 5,000 effective events defined by $n_{\text{eff}} \equiv (\Sigma_i w_i)^2 / \Sigma_i w_i^2$. This ensures that the bin has sufficient statistical precision, which is important for stabilizing the bin uncertainties.
3. **Sufficient data statistical precision:** Each bin should further have a data statistical uncertainty smaller than $15\%$, which ensures that the kinematic region probed has sufficient support by the data. This criterion is most likely fulfilled when $n_{\text{eff}}$ > 5,000.
4. **Validation using the pseudo-measurement:** Any analysis should always be checked using the pseudo-measurement against the target, to ensure a reliable result is obtained before drawing any conclusion based on the measurement with real data. Examples of such tests are performed in [2_pseudo_results.ipynb](2_pseudo_results.ipynb). The test performed as part of the example code is a standard, binned $\chi^2$ compatibility test between the pseudo-measurement and the target. The key step of this procedure is to construct the measurement covariance matrix that encodes the correlation between bins. The $\chi^2$ and associated $p$-value are then obtained with a few lines of code that perform matrix operations. The outcome of these tests will depend on the assumed correlation model as discussed in the next bullet.
5. **Alternative treatment of uncertainty correlation:** The default recommended procedure is to treat each systematic uncertainty component as fully correlated across bins. However, this might not be ideal for two-point systematic uncertainties, for which the internal uncertainty correlation is not known. Certain regions of phase space have dominant uncertainty from the two unfolding systematic uncertainty sources, which both are two-point systematic uncertainties. This can in particular be an issue when using $m_{j2}$, where the uncertainty amplitude has a strong shape dependence, and a switch in sign in the middle of the distribution. In cases like this, poor $p$-values may sometimes result from assuming bins are fully correlated. In these cases, other correlation models may be used. As the true correlation model is not known, any correlation assumption must be tested on the pseudodata before it can be used to extract information from the actual data. If few bins are used, one can treat the bins as fully uncorrelated for this uncertainty component. For instance, if four bins are used, the systematic uncertainty can be split into four components ("nuisance parameters"). More sophisticated decorrelation schemes could be explored for which each uncertainty component has a smooth uncertainty amplitude spanning the full range where at any point (i.e. for any bin) this uncertainty adds in quadrature to match the total uncertainty of the two-point systematics.

## Citing this work

BibTeX citation for the paper: 
```
@techreport{Aad:2899105,
      author        = "ATLAS Collaboration",
      collaboration = "ATLAS",
      title         = "{A simultaneous unbinned differential cross section
                       measurement of twenty-four $Z$+jets kinematic observables
                       with the ATLAS detector}",
      institution   = "CERN",
      archivePrefix = "arXiv",
      eprint        = "2405.20041",
      reportNumber  = "CERN-EP-2024-132",
      address       = "Geneva",
      year          = "2024",
      url           = "https://cds.cern.ch/record/2899105",
      note          = "33 pages in total, author list starting on page 16, 3
                       figures, 0 tables, submitted to PRL. All figures including auxiliary figures are available at http://atlas.web.cern.ch/Atlas/GROUPS/PHYSICS/PAPERS/STDM-2020-17",
}
```

BibTeX citation for the dataset:
```
@dataset{atlas_collaboration_2024_11507450,
  author       = {ATLAS Collaboration},
  title        = {ATLAS OmniFold 24-Dimensional Z+jets Open Data},
  month        = jun,
  year         = 2024,
  publisher    = {Zenodo},
  doi          = {10.5281/zenodo.11507450},
  url          = {https://doi.org/10.5281/zenodo.11507450}
}
```